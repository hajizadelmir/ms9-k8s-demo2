package az.ingress.ms9k8sdemo2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms9K8sDemo2Application {

	public static void main(String[] args) {
		SpringApplication.run(Ms9K8sDemo2Application.class, args);
	}

}
