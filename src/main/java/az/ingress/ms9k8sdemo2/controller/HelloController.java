package az.ingress.ms9k8sdemo2.controller;

import az.ingress.ms9k8sdemo2.dto.HelloDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {

    @GetMapping
    public HelloDto sayHello(){
        var a =new HelloDto();
        a.setMessage("Hello from controller");
        return a;
    }

}
